<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<script src="/public/scripts/jquery.js"></script>
	<script src="/public/scripts/form.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/styles/style.css">
	<link href="/public/styles/css/bootstrap.min.css" rel="stylesheet">
	<link href="/public/styles/css/signin.css" rel="stylesheet">
</head>
<body>
<?php echo $content; ?>
</body>
</html>