<?php 

return [
	'all' => [
		'login',
		
	],
	'authorize' => [
		//
	],
	'guest' => [
		//
	],
	'admin' => [
		'posts',
		'add',
		'edit',
		'delete',
		'logout',
	],
];