<?php 

namespace application\models;

use application\core\Model; 



class Admin extends Model  
{

	public $error;

	public function loginValidate($post) {
		$config = require 'application/config/admin.php';
		if($config['login'] != $post['login'] or $config['password'] != $post['password']){
			$this->error = 'Login yoki parol xato!';
		return false;
		}
		return true;
	}

	public function postValidate($post, $type){
	$nameLen = iconv_strlen($post['name']);
	$descLen = iconv_strlen($post['desc']);
	$textLen = iconv_strlen($post['text']);
		if($nameLen < 3 or $nameLen > 100) {
			$this->error = 'Ism 3 dan 20 gacha simvoldan ibora bo`lishi lozim!';
			return false;
		} else if($descLen < 3 or $descLen > 100) {
			$this->error = 'Desc 3 dan 20 gacha simvoldan ibora bo`lishi lozim!';
			return false;
		} else if($textLen < 10 or $textLen > 5000) {
			$this->error = 'Xabar 10 dan 5000 gacha simvoldan ibora bo`lishi lozim!';
			return false;

		}

		 if(empty($_FILES['img']['tmp_name']) and $type == 'add'){
		 	$this->error = 'Foto tanlanmadi!';
		 	return false;
		 	}
		return true;
	}


	public function postAdd($post){
		$params = [
			'id' => '',
			'name' => $post['name'],
			'desc' => $post['desc'],
			'text' => $post['text'],
		];
		$this->db->query('INSERT INTO posts VALUES(:id, :name, :desc, :text)', $params);
		return $this->db->lastInsertId();
	}

	public function postUploadImage($path, $id){
		move_uploaded_file($path, 'public/materials/'.$id.'.jpg');
	}

	public function isPostExists($id){
		$params = [
			'id' => $id,
		];
		return $this->db->column('SELECT id FROM posts WHERE id = :id', $params);
	}

	


}