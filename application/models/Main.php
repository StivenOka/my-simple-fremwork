<?php 

namespace application\models;

use application\core\Model; 

class Main extends Model  
{

	public $error;

	public function contactValidate($post) {
		$nameLen = iconv_strlen($post['name']);
		$textLen = iconv_strlen($post['text']);
		if($nameLen < 3 or $nameLen > 20) {
			$this->error = 'Ism 3 dan 20 gacha simvoldan ibora bo`lishi lozim!';
			return false;
		} else if(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
			$this->error = 'E-mail noto`g`ri ko`rsatildi!';

			return false;
		} else if($textLen < 10 or $textLen > 500) {
			$this->error = 'Xabar 10 dan 500 gacha simvoldan ibora bo`lishi lozim!';
			return false;

		}
		return true;
	}	

}