<?php 

namespace application\controllers;

use application\core\Controller; 

class AdminController extends Controller{
		
	public function __construct($route){
		parent::__construct($route);
		$this->view->layout = 'admin'; 
	}


	public function loginAction(){
		if(isset($_SESSION['admin'])){
			$this->view->redirect('admin/add');
		}
		if(!empty($_POST)){
			if(!$this->model->loginValidate($_POST)){
				$this->view->message('error', $this->model->error);
			}
			$_SESSION['admin'] = true;

			$this->view->location('admin/add');
		}
		$this->view->render('Adminga kirish');
	}
	public function addAction(){
		if(!empty($_POST)){
			if(!$this->model->postValidate($_POST, 'add')){
				$this->view->message('error', $this->model->error);
			}
			$id = $this->model->postAdd($_POST);
			if(!$id){
				$this->view->message('error', 'Xato !!!!');
			}
			$this->model->postUploadImage($_FILES['img']['tmp_name'], $id);
			$this->view->message('success', 'Post qo`shildi');

		}
		$this->view->render('Post qoshish');
	}

	public function editAction(){
		if(!empty($_POST)){
			if(!$this->model->postValidate($_POST, 'edit')){
				$this->view->message('error', $this->model->error);
			}
			$this->view->message('success', 'OK');
		}
		$this->view->render('O`zgartirish');
	}

	public function deleteAction(){
		debug($this->route);
	}

	public function logoutAction(){
		unset($_SESSION['admin']);
		$this->view->redirect('admin/login');
	}

	public function postsAction(){
		$this->view->render('Posts');
	}
}